@api @pet
Feature: Pet

  @deletePet
  Scenario: Post request for pet endpoint
  Acceptance: To create a pet with mandatory fields which are ID, NAME, STATUS. You should receive server error when one of these fields is missing
    When I send POST request to pet endpoint with following pet data:
      | id | name    | status    |
      | 3  | Prenses | available |

    Then status code is 200

    And response body for creating pet should be:
      | id | name    | status    |
      | 3  | Prenses | available |


  @getRequest @deletePet
  Scenario: Get request for pet endpoint
    Given I have a pet with following pet data:
      | id | name   | status |
      | 11 | Bulent | died   |

    When I send GET request for 'pet' with id: 11

    Then status code is 200

    And response body for GET pet should be:
      | id | name   | status |
      | 11 | Bulent | died   |


#curl -X 'POST' \
#  'https://petstore.swagger.io/v2/pet' \
#  -H 'accept: application/json' \
#  -H 'Content-Type: application/json' \
#  -d '{
#  "id": 19,
#  "name": "Bulent",
#  "status": "available"
#}'